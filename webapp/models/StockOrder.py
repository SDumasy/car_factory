from database import db
class StockOrder(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.Integer())
    milk = db.Column('milk', db.Float)
    hides = db.Column('hides', db.Float)
    milk_processed = db.Column('milk_processed', db.Boolean)
    hides_processed = db.Column('hides_processed', db.Boolean)
    customer = db.Column('customer', db.String(32))

    def __repr__(self):
        return "<Order(time='%s', milk='%s', hides='%s', customer='%s')>" % (
            self.time, self.milk, self.hides, self.customer)
