import unittest
import app as myapi
import json
import sys

class TestYak3(unittest.TestCase):

    def setUp(self):
        self.app = myapi.app.test_client()

    def load_basic_herd(self):
        """Loads in a basic herd"""
        xml = """<herd>
            <labyak name="Betty-1" age="4" sex="f"/>
            <labyak name="Betty-2" age="8" sex="f"/>
            <labyak name="Betty-3" age="9.5" sex="f"/>
        </herd>"""
        return self.app.post('/yak-shop/load', data=xml)

    def load_new_herd(self):
        """Loads in a basic herd"""
        xml = """<herd>
            <labyak name="Betty-1" age="4" sex="f"/>
            <labyak name="Betty-2" age="8" sex="m"/>
            <labyak name="Betty-3" age="0.8" sex="f"/>
            <labyak name="Betty-4" age="9.9" sex="m"/>
            <labyak name="Betty-5" age="9.5" sex="f"/>
        </herd>"""
        return self.app.post('/yak-shop/load', data=xml)



    def test_yak_stock_1(self):
        """Test load yak stock request with following herd and time"""
        response = self.load_basic_herd()
        self.assertEqual(response.status_code, 205)
        stock_response = self.app.get('/yak-shop/stock/13')
        self.assertEqual(stock_response.data, """{"milk" : 1104.48, "skins" : 3}""")

    def test_yak_stock_2(self):
        """Test load yak stock request with herd and time"""
        response = self.load_basic_herd()
        self.assertEqual(response.status_code, 205)
        self.app.get('/yak-shop/stock/13')
        stock_response = self.app.get('/yak-shop/stock/14')
        self.assertEqual(stock_response.data, """{"milk" : 1188.81, "skins" : 4}""")

    def test_yak_stock_3(self):
        """Test load yak stock request with following herd and time"""
        response = self.load_new_herd()
        self.assertEqual(response.status_code, 205)
        stock_response = self.app.get('/yak-shop/stock/100')
        self.assertEqual(stock_response.data, """{"milk" : 11954.4, "skins" : 26}""")

    def test_yak_stock_4(self):
        """Test load yak stock request with following herd and time"""
        response = self.load_new_herd()
        self.assertEqual(response.status_code, 205)
        stock_response = self.app.get('/yak-shop/stock/200')
        self.assertEqual(stock_response.data, """{"milk" : 21768.9, "skins" : 49}""")

    def test_yak_herd_1(self):
        """Test query herd request with following herd and time"""
        response = self.load_basic_herd()
        self.assertEqual(response.status_code, 205)
        stock_response = self.app.get('/yak-shop/herd/13')
        self.assertEqual(stock_response.data, """{"dead" : [], "herd" : [{"age" : 4.13, "age-last-shaved" : 400, "name" : "Betty-1"}, {"age" : 8.13, "age-last-shaved" : 800, "name" : "Betty-2"}, {"age" : 9.63, "age-last-shaved" : 950, "name" : "Betty-3"}]}""")

    def test_yak_herd_2(self):
        """Test query herd request with herd and time"""
        response = self.load_basic_herd()
        self.assertEqual(response.status_code, 205)
        self.app.get('/yak-shop/herd/13')
        stock_response = self.app.get('/yak-shop/herd/14')
        self.assertEqual(stock_response.data, """{"dead" : [], "herd" : [{"age" : 4.14, "age-last-shaved" : 413, "name" : "Betty-1"}, {"age" : 8.14, "age-last-shaved" : 800, "name" : "Betty-2"}, {"age" : 9.64, "age-last-shaved" : 950, "name" : "Betty-3"}]}""")

    def test_yak_herd_3(self):
        """Test query herd request with following herd and time"""
        response = self.load_new_herd()
        self.assertEqual(response.status_code, 205)
        stock_response = self.app.get('/yak-shop/herd/100')
        self.assertEqual(stock_response.data, """{"dead" : [{"age" : 10.0, "age-last-shaved" : 990, "name" : "Betty-4"}, {"age" : 10.0, "age-last-shaved" : 986, "name" : "Betty-5"}], "herd" : [{"age" : 5.0, "age-last-shaved" : 491, "name" : "Betty-1"}, {"age" : 9.0, "age-last-shaved" : 885, "name" : "Betty-2"}, {"age" : 1.8, "age-last-shaved" : 170, "name" : "Betty-3"}]}""")

    def test_yak_herd_4(self):
        """Test query herd request with following herd and time"""
        response = self.load_new_herd()
        self.assertEqual(response.status_code, 205)
        stock_response = self.app.get('/yak-shop/herd/200')
        self.assertEqual(stock_response.data, """{"dead" : [{"age" : 10.0, "age-last-shaved" : 993, "name" : "Betty-2"}, {"age" : 10.0, "age-last-shaved" : 990, "name" : "Betty-4"}, {"age" : 10.0, "age-last-shaved" : 986, "name" : "Betty-5"}], "herd" : [{"age" : 6.0, "age-last-shaved" : 589, "name" : "Betty-1"}, {"age" : 2.8, "age-last-shaved" : 278, "name" : "Betty-3"}]}""")

    def test_yak_herd_5(self):
        """Test query herd request with following herd and time"""
        response = self.load_new_herd()
        self.assertEqual(response.status_code, 205)
        stock_response = self.app.get('/yak-shop/herd/200f')
        self.assertEqual(stock_response.status_code, 400)

if __name__ == '__main__':
    unittest.main()
