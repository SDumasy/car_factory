import sys
import xmltodict
from models.Yak import Yak

xmlfolder = "xml/"
max_yak_age = 1000
min_yak_shave_age = 100

def calculate_produced_milk(time, begin_age):
    """Calculates the amount of milk a yak produces in a time span"""
    total_milk = 0
    age = begin_age
    for i in range(0, time):
        if age >= max_yak_age:
            return total_milk
        total_milk += 50 - age * 0.03
        age += 1
    return total_milk

def calculate_shaved_hides(start_time, end_time, yak):
    """Calculates the amount of shaved hides a yak produces in a time span"""
    total_hides = 0
    age = yak.age
    for i in range(start_time, end_time):
        if age >= max_yak_age:
            return total_hides
        if age >= min_yak_shave_age and 8 + age * 0.01 < age - yak.age_last_shaved:
            yak.age_last_shaved = age
            total_hides += 1

        age += 1

    return total_hides

def parse_xml(xml):
    """Parses an herd xml"""
    content_dict = xmltodict.parse(xml)
    yak_list = []
    for herd, herd_dict in content_dict.items():
        for xmlrow, labyak_list, in herd_dict.items():

            # Loop through the labyak rows
            for labyak in labyak_list:

                # Get attributes from dict
                name = labyak['@name']
                begin_age = float(labyak['@age']) * 100
                sex = labyak['@sex']
                age_last_shaved = - max_yak_age

                yak = Yak(name = name.encode("utf-8"), age = begin_age, sex = sex,
                          age_last_shaved = age_last_shaved)
                yak_list.append(yak)

    return yak_list

def query(start_time, end_time, yak_list):
    """Calculates the milk and hides for a time span from a Yak herd"""

    total_milk = 0
    total_hides = 0

    for yak in yak_list:

        # Calculate milk and hides for 1 labyak
        total_milk += calculate_produced_milk(end_time - start_time, yak.age)
        total_hides += calculate_shaved_hides(start_time, end_time, yak)

        # Add elapsed time to yak age and cap at 10 years
        age = yak.age + (end_time - start_time)
        if age > 1000:
            age = 1000
        yak.age = age

    return (total_milk, total_hides)

if __name__ == '__main__':

    # Get system arguments
    _, time, xmlfile = sys.argv
    time = int(time)

    # Open xml file
    file = open(xmlfolder + xmlfile, "r")
    xml = file.read()
    yak_list = parse_xml(xml)
    total_milk, total_hides = query(0, time, yak_list)

    # Print output
    print("In Stock:")
    print(str(total_milk) + " liters of milk")
    print(str(total_hides) + " skins of wool")
    print("Herd:")
    for yak in yak_list:
        print(yak.name + " " + str(yak.age / 100) + " years old")
