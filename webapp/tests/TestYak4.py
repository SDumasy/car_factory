import unittest
import app as myapi
import json
import TestYak3
import sys

class TestYak4(unittest.TestCase):

    def setUp(self):
        self.app = myapi.app.test_client()

    def load_basic_herd(self):
        """Loads in a basic herd"""
        xml = """<herd>
            <labyak name="Betty-1" age="4" sex="f"/>
            <labyak name="Betty-2" age="8" sex="f"/>
            <labyak name="Betty-3" age="9.5" sex="f"/>
        </herd>"""
        return self.app.post('/yak-shop/load', data=xml)

    def load_new_herd(self):
        """Loads in a basic herd"""
        xml = """<herd>
            <labyak name="Betty-1" age="4" sex="f"/>
            <labyak name="Betty-2" age="8" sex="m"/>
            <labyak name="Betty-3" age="0.8" sex="f"/>
            <labyak name="Betty-4" age="9.9" sex="m"/>
            <labyak name="Betty-5" age="9.5" sex="f"/>
        </herd>"""
        return self.app.post('/yak-shop/load', data=xml)



    def test_yak_order_1(self):
        """Test yak order request with following herd and time"""
        response = self.load_basic_herd()
        json = """{"customer" : "Medvedev", "order" : { "milk" : 1100, "skins" : 3 } }"""
        self.assertEqual(response.status_code, 205)
        order_response = self.app.post('/yak-shop/order/14', data=json)
        self.assertEqual(order_response.status_code, 201)
        self.assertEqual(order_response.data, """{"milk" : 1100, "skins" : 3}""")

    def test_yak_order_2(self):
        """Test yak order request with following herd and time"""
        response = self.load_basic_herd()
        json = """{"customer" : "Medvedev2", "order" : { "milk" : 1100, "skins" : 5 } }"""
        self.assertEqual(response.status_code, 205)
        order_response = self.app.post('/yak-shop/order/14', data=json)
        self.assertEqual(order_response.status_code, 206)
        self.assertEqual(order_response.data, """{"milk" : 1100}""")

    def test_yak_order_3(self):
        """Test yak order request with following herd and time"""
        response = self.load_basic_herd()
        json = """{"customer" : "Medvedev2", "order" : { "milk" : 1500, "skins" : 8 } }"""
        self.assertEqual(response.status_code, 205)
        order_response = self.app.post('/yak-shop/order/14', data=json)
        self.assertEqual(order_response.status_code, 404)
        self.assertEqual(order_response.data, """{}""")

    def test_yak_order_4(self):
        """Test order request with following herd and time"""
        response = self.load_new_herd()
        json = """{"customer" : "Medvedev2", "order" : { "milk" : 1500, "skins" : 8 } }"""
        self.assertEqual(response.status_code, 205)
        stock_response = self.app.post('/yak-shop/order/200o', data=json)
        self.assertEqual(stock_response.status_code, 400)

    def test_yak_order_5(self):
        """Test yak order request with following herd and time"""
        response = self.load_basic_herd()
        json = """{"customer" : "Medvedev2", "order" : { "milk" : 1, "skins" : 1 } }"""
        self.assertEqual(response.status_code, 205)
        for i in range(0, 4):
            order_response = self.app.post('/yak-shop/order/14', data=json)
            self.assertEqual(order_response.status_code, 201)
            self.assertEqual(order_response.data, """{"milk" : 1, "skins" : 1}""")

        order_response = self.app.post('/yak-shop/order/14', data=json)
        self.assertEqual(order_response.status_code, 206)
        self.assertEqual(order_response.data, """{"milk" : 1}""")

    def test_yak_order_6(self):
        """Test yak order request with following herd and time"""
        response = self.load_basic_herd()
        json = """{"customer" : "Medvedev2", "order" : { "milk" : 100, "skins" : 100 } }"""
        self.assertEqual(response.status_code, 205)
        order_response = self.app.post('/yak-shop/order/10000', data=json)
        self.assertEqual(order_response.status_code, 206)
        self.assertEqual(order_response.data, """{"milk" : 100}""")


if __name__ == '__main__':
    unittest.main()
