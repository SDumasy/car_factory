## Running the application

Install docker then:

```bash
Docker-compose build
Docker-compose up
```
Application is available under localhost:5000


## Running story 1

```bash
docker run car-factory_web python /opt/webapp/xmlquerier.py 14 herd1.xml
```

## Running a test class
```bash
docker run car-factory_web python /opt/webapp/tests/TestYak4.py
```
