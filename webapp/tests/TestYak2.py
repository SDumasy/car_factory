import unittest
import app as myapi
import json
import sys

class TestYak2(unittest.TestCase):
    def setUp(self):
        self.app = myapi.app.test_client()

    def test_yak_load_1(self):
        """Test load  yak post request with following xml"""
        xml = """<herd>
            <labyak name="Betty-1" age="4" sex="f"/>
            <labyak name="Betty-2" age="8" sex="f"/>
            <labyak name="Betty-3" age="9.5" sex="f"/>
        </herd>"""
        response = self.app.post('/yak-shop/load', data=xml)
        self.assertEqual(response.status_code, 205)


    def test_yak_load_2(self):
        """Test load  yak post request with following xml"""
        xml = """herd>
            labyak name="Betty-1" age="4" sex="f"/>
            <labyak name="Betty-2" age="8" sex="f"/>
            <labyak name="Betty-3" age="9.5" sex="f"/>
        </herd>"""
        response = self.app.post('/yak-shop/load', data=xml)
        self.assertEqual(response.status_code, 400)

    def test_yak_load_3(self):
        """Test load  yak post request with following xml"""
        xml = """<herd>
            <labyak name="Betty-2" age="8" sex="f"/>
            <labyak name="Betty-3" age="9.5" sex="f"/>
        </herd>"""
        response = self.app.post('/yak-shop/load', data=xml)
        self.assertEqual(response.status_code, 205)

    def test_yak_load_4(self):
        """Test load  yak post request with following xml"""
        xml = """<herd>
            <labyak name="Betty-5" age="5" sex="m"/>
            <labyak name="Betty-3" age="8.5" sex="f"/>
        </herd>"""
        response = self.app.post('/yak-shop/load', data=xml)
        self.assertEqual(response.status_code, 205)
if __name__ == '__main__':
    unittest.main()
