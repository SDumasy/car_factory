from database import db
class Stock(db.Model):
    time = db.Column(db.Integer(), primary_key=True)
    milk = db.Column('milk', db.Float)
    hides = db.Column('hides', db.Float)

    def __repr__(self):
        return "<Stock(time='%s', milk='%s', hides='%s')>" % (
            self.time, self.milk, self.hides)
