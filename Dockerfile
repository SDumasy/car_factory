# Take the latest alpine docker image
FROM alpine:latest

# Install python with and pip
RUN apk add --no-cache --update python py-pip bash \
&& apk add --no-cache py-psycopg2
ADD ./webapp/requirements.txt /tmp/requirements.txt

# Install python dependencies specified in requirments.txt
RUN pip install --no-cache-dir -q -r /tmp/requirements.txt

# Change workdir and add the code to it
ADD ./webapp /opt/webapp/
WORKDIR /opt/webapp
ENV PYTHONPATH "${PYTONPATH}:/opt/webapp"


# HEROKU REQUIREMENT: Run the image as a non-root user
RUN adduser -D myuser
USER myuser

# Run the app, CMD is required to run on Heroku
# $PORT is set by Heroku
# wsgi.py contains the app object to be used by the server
CMD gunicorn --bind 0.0.0.0:$PORT wsgi

