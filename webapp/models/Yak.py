from database import db
import json
class Yak(db.Model):
    name = db.Column(db.String(32), primary_key=True)
    age = db.Column('age', db.Integer)
    age_last_shaved = db.Column('age_last_shaved', db.Integer)
    sex = db.Column('sex', db.String(32))

    def __repr__(self):
        return "<Yak(name='%s', age='%s', sex='%s')>" % (
            self.name, self.age, self.sex)

