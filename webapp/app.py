import os, urlparse
from flask import Flask
from flask import request
from flask import Response
from models.Yak import Yak
from models.StockOrder import StockOrder
from models.Stock import Stock
import xmlquerier as query
import json
import collections
from werkzeug.exceptions import BadRequest

def create_app():
    """Create the app and database config"""
    app = Flask(__name__)
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    # If ran remotely on Heroku
    if 'DATABASE_URL' in os.environ:
        heroku = urlparse.urlparse(os.environ['DATABASE_URL'])
        app.config['SQLALCHEMY_DATABASE_URI'] = "postgres://" + heroku.username + ":" + heroku.password + "@" + heroku.hostname + ":" + str(heroku.port) + "/" + heroku.path[1:]
    elif(os.environ.get('POSTGRES_USER') != None):
        app.config['SQLALCHEMY_DATABASE_URI'] = "postgres://" + os.environ['POSTGRES_USER'] + ":" + os.environ['POSTGRES_PASSWORD'] + "@" + os.environ['POSTGRES_HOST'] + ":" + os.environ['POSTGRES_PORT'] + "/" + os.environ['POSTGRES_DB']
    return app

app = create_app()
from models.database import db
db.init_app(app)

def query_on_time(time):
    """Performs a query on specific time and updates databases with
        newly calculated values"""
    try:
        time = int(time)
    except:
        raise BadRequest("Time variable is not an integer")

    yak_list = Yak.query.all()
    last_stocks = Stock.query.order_by(- Stock.time).first()

    if(last_stocks.time == time):
        return (last_stocks.milk, last_stocks.hides, yak_list)
    elif(last_stocks.time > time):
        raise BadRequest('Time has to be larger than earlier queried time')
    else:
        new_milk, new_hides = query.query(last_stocks.time, int(time), yak_list)

        total_milk = last_stocks.milk + new_milk
        total_hides = last_stocks.hides + new_hides

        # Save a new stock in the database
        stock = Stock(time = time, milk = total_milk, hides = total_hides)
        db.session.add(stock)

        # Update values of yaks
        for yak in yak_list:
            db.session.add(yak)

        db.session.commit()

        return (total_milk, total_hides, yak_list)


@app.route('/yak-shop/stock/<time>')
def get_stock(time):
    """ Gets a json dump of the stocks at specified time"""
    total_milk, total_hides, _ = query_on_time(time)

    # Dict for json dump
    stock_dict = {"milk": total_milk, "skins": int(total_hides)}

    return json.dumps(stock_dict, separators=(', ', ' : '))

@app.route('/yak-shop/herd/<time>')
def get_herd(time):
    """ Gets a json dump of the herd at specified time"""

    _ , _ , yak_list =  query_on_time(time)

    list = []
    dead_list = []
    for yak in yak_list:
        dict = {"name":yak.name, "age":yak.age / 100.0, "age-last-shaved":yak.age_last_shaved}
        if yak.age == 1000:
            dead_list.append(dict)
        else:
            list.append(dict)

    # Sort lists by name
    sorted_list = sorted(list, key=lambda k: k['name'])
    sorted_dead_list = sorted(dead_list, key=lambda k: k['name'])

    return json.dumps({"herd":sorted_list, "dead":sorted_dead_list}, separators=(', ', ' : '))


@app.route('/yak-shop/order/<time>', methods=['POST'])
def order_goods(time):
    """Order goods request and deduct it from the stock"""
    milk, hides, _ =  query_on_time(time)

    try:
        order = json.loads(request.data)

        order = StockOrder(time = int(time), customer=order['customer'], hides=order['order']['skins'],
                      milk=order['order']['milk'])
    except:
        raise BadRequest("Incorrect post body")

    new_milk = milk - order.milk
    new_hides = hides - order.hides
    dump_dict = {}
    if new_milk >= 0:
        order.milk_processed = True
        dump_dict["milk"] = order.milk
    else:
        new_milk = milk
        order.milk_processed = False

    if new_hides >= 0:
        order.hides_processed = True
        dump_dict["skins"] = order.hides
    else:
        new_hides = hides
        order.hides_processed = False

    stock = Stock.query.filter_by(time=int(time)).first()
    stock.milk = new_milk
    stock.hides = new_hides
    db.session.add(stock)
    db.session.add(order)
    db.session.commit()

    if order.hides_processed and order.milk_processed:
        return json.dumps(dump_dict, separators=(', ', ' : ')), 201
    elif order.hides_processed or order.milk_processed:
        return json.dumps(dump_dict, separators=(', ', ' : ')), 206
    else:
        return json.dumps(dump_dict, separators=(', ', ' : ')), 404

@app.route('/yak-shop/load', methods=['POST'])
def post_yaks():
    """Reset the database and parse yaks xml and add to database"""
    db.drop_all()
    db.create_all()

    try:
        yak_list = query.parse_xml(request.data)
    except:
        raise BadRequest("Incorrect post body")

    stock = Stock(time=0, milk=0, hides=0)
    db.session.add(stock)

    for yak in yak_list:
        db.session.add(yak)
    db.session.commit()
    return Response(status=205)

if __name__ == '__main__':
    app.run(host='0.0.0.0')

