import unittest
import xmlquerier as query
import json
from models.Yak import Yak

import sys

class TestYak1(unittest.TestCase):

    def test_calculate_milk_1(self):
        """Test milk method for time = 5 and age = 0 days"""
        milk = query.calculate_produced_milk(5, 0)
        self.assertEqual(milk, (50 - 0 * 0.03) + (50 - 1 * 0.03) +
                         (50 - 2 * 0.03) + (50 - 3 * 0.03) + (50 - 4 * 0.03))

    def test_calculate_milk_2(self):
        """Test milk method for time = 8 and age = 100 days"""
        milk = query.calculate_produced_milk(8, 100)
        self.assertEqual(milk, (50 - 100 * 0.03) + (50 - 101 * 0.03) +
                         (50 - 102 * 0.03) + (50 - 103 * 0.03) + (50 - 104 * 0.03) +
                         (50 - 105 * 0.03) + (50 - 106 * 0.03) + (50 - 107 * 0.03))

    def test_calculate_milk_3(self):
        """Test milk method for time = 8 and age = 995 days"""
        milk = query.calculate_produced_milk(8, 995)
        self.assertEqual(milk, (50 - 995 * 0.03) + (50 - 996 * 0.03) +
                         (50 - 997 * 0.03) + (50 - 998 * 0.03) + (50 - 999 * 0.03))

    def test_calculate_milk_4(self):
        """Test milk method for time = 1 and age = 500 days"""
        milk = query.calculate_produced_milk(1, 500)
        self.assertEqual(milk, (50 - 500 * 0.03))


    def test_calculate_milk_5(self):
        """Test milk method for time = 0 and age = 600 days"""
        milk = query.calculate_produced_milk(0, 600)
        self.assertEqual(milk, 0)


    def test_calculate_hides_1(self):
        """Test hides method for time = 0 and age = 600 days"""
        hides = query.calculate_shaved_hides(0, 0, Yak(age=600, age_last_shaved=-1000))
        self.assertEqual(hides, 0)


    def test_calculate_hides_2(self):
        """Test hides method for time = 50 and age = 0 days"""
        hides = query.calculate_shaved_hides(0, 50, Yak(age=0, age_last_shaved=-1000))
        self.assertEqual(hides, 0)


    def test_calculate_hides_3(self):
        """Test hides method for time = 5 and age = 500 days"""
        hides = query.calculate_shaved_hides(0, 5, Yak(age=500, age_last_shaved=-1000))
        self.assertEqual(hides, 1)


    def test_calculate_hides_4(self):
        """Test hides method for time = 13 and age = 413 days"""
        hides = query.calculate_shaved_hides(0, 13, Yak(age=413, age_last_shaved=-1000))
        self.assertEqual(hides, 1)


    def test_calculate_hides_5(self):
        """Test hides method for time = 13 and age = 413 days"""
        hides = query.calculate_shaved_hides(0, 14, Yak(age=413, age_last_shaved=-1000))
        self.assertEqual(hides, 2)


    def test_parse_xml_1(self):
        """Test the parse xml method"""
        xml_string = """<herd>
            <labyak name="Betty-1" age="4" sex="f"/>
            <labyak name="Betty-2" age="8" sex="f"/>
            <labyak name="Betty-3" age="9.5" sex="f"/>
        </herd>"""
        yak_list = query.parse_xml(xml_string)
        for yak in yak_list:
            self.assertEqual(yak.sex, "f")
            if yak.name == "Betty-1":
                self.assertEqual(yak.age, 400)
            if yak.name == "Betty-2":
                self.assertEqual(yak.age, 800)
            if yak.name == "Betty-3":
                self.assertEqual(yak.age, 950)

    def test_parse_xml_2(self):
        """Test the parse xml method"""
        xml_string = """<herd>
            <labyak name="Betty-1" age="4" sex="f"/>
            <labyak name="Betty-2" age="8" sex="f"/>
            <labyak name="Betty-3" age="9.5" sex="f"/>
            <labyak name="Betty-4" age="9.5" sex="f"/>
            <labyak name="Betty-5" age="9.5" sex="f"/>
            <labyak name="Betty-6" age="9.5" sex="f"/>
        </herd>"""
        yak_list = query.parse_xml(xml_string)
        for yak in yak_list:
            self.assertEqual(yak.sex, "f")
            if yak.name == "Betty-1":
                self.assertEqual(yak.age, 400)
            if yak.name == "Betty-2":
                self.assertEqual(yak.age, 800)
            if yak.name in ["Betty-3", "Betty-4", "Betty-5", "Betty-6"]:
                self.assertEqual(yak.age, 950)

    def test_parse_xml_3(self):
        """Test the parse xml method"""
        xml_string = """<herd>
            <labyak name="Betty-1" age="4.5" sex="m"/>
            <labyak name="Betty-2" age="8" sex="m"/>
            <labyak name="Betty-3" age="9.5" sex="f"/>
            <labyak name="Betty-4" age="9.5" sex="f"/>
            <labyak name="Betty-5" age="9.5" sex="f"/>
            <labyak name="Betty-6" age="9.5" sex="f"/>
        </herd>"""
        yak_list = query.parse_xml(xml_string)
        for yak in yak_list:
            if yak.name == "Betty-1":
                self.assertEqual(yak.sex, "m")
                self.assertEqual(yak.age, 450)
            if yak.name == "Betty-2":
                self.assertEqual(yak.sex, "m")
                self.assertEqual(yak.age, 800)
            if yak.name in ["Betty-3", "Betty-4", "Betty-5", "Betty-6"]:
                self.assertEqual(yak.sex, "f")
                self.assertEqual(yak.age, 950)



    def test_query_1(self):
        """Test read_xml method for time = 13 and the xml string below"""
        xml_string = """<herd>
            <labyak name="Betty-1" age="4" sex="f"/>
            <labyak name="Betty-2" age="8" sex="f"/>
            <labyak name="Betty-3" age="9.5" sex="f"/>
        </herd>"""
        time = 13
        yak_list = query.parse_xml(xml_string)
        milk, hides = query.query(0, time, yak_list)
        self.assertEqual(hides, 3)
        self.assertEqual(milk, 1104.480)
        for yak in yak_list:
            if yak.name == "Betty-1":
                self.assertEqual(yak.age, 413)
            if yak.name == "Betty-2":
                self.assertEqual(yak.age, 813)
            if yak.name == "Betty-3":
                self.assertEqual(yak.age, 963)


    def test_query_2(self):
        """Test read_xml method for time = 14 and the xml string below"""
        xml_string = """<herd>
            <labyak name="Betty-1" age="4" sex="f"/>
            <labyak name="Betty-2" age="8" sex="f"/>
            <labyak name="Betty-3" age="9.5" sex="f"/>
        </herd>"""
        time = 14
        yak_list = query.parse_xml(xml_string)
        milk, hides = query.query(0, time, yak_list)
        self.assertEqual(hides, 4)
        self.assertEqual(milk, 1188.810)
        for yak in yak_list:
            if yak.name == "Betty-1":
                self.assertEqual(yak.age, 414)
            if yak.name == "Betty-2":
                self.assertEqual(yak.age, 814)
            if yak.name == "Betty-3":
                self.assertEqual(yak.age, 964)


    def test_query_3(self):
        """Test read_xml method for time = 5 and the xml string below"""
        xml_string = """<herd>
            <labyak name="Betty-1" age="0" sex="f"/>
            <labyak name="Betty-2" age="0" sex="f"/>
            <labyak name="Betty-3" age="0" sex="f"/>
        </herd>"""
        time = 5
        yak_list = query.parse_xml(xml_string)
        milk, hides = query.query(0, time, yak_list)
        self.assertEqual(hides, 0)
        self.assertEqual(milk, 3 * ((50 - 0 * 0.03) + (50 - 1 * 0.03) +
                         (50 - 2 * 0.03) + (50 - 3 * 0.03) + (50 - 4 * 0.03)))
        for yak in yak_list:
            self.assertEqual(yak.age, 5)

    def test_query_4(self):
        """Test read_xml method for time = 5 and the xml string below"""
        xml_string = """<herd>
            <labyak name="Betty-1" age="0" sex="f"/>
            <labyak name="Betty-2" age="0" sex="f"/>
            <labyak name="Betty-3" age="9.99" sex="f"/>
        </herd>"""
        time = 5
        yak_list = query.parse_xml(xml_string)
        milk, hides = query.query(0, time, yak_list)
        self.assertEqual(hides, 1)
        self.assertEqual(milk, (50 - 999 * 0.03) + 2.0 * ((50.0 - 0.0 * 0.03) + (50.0 - 1.0 * 0.03) +
                                    (50.0 - 2.0 * 0.03) + (50.0 - 3.0 * 0.03) + (50.0 - 4.0 * 0.03)))
        for yak in yak_list:
            if yak.name in ["Betty-1", "Betty-2"]:
                self.assertEqual(yak.age, 5)
            elif yak.name == "Betty-3":
                self.assertEqual(yak.age, 1000)

    def test_query_5(self):
        """Test read_xml method for time = 5 and the xml string below"""
        xml_string = """<herd>
            <labyak name="Betty-1" age="0" sex="f"/>
            <labyak name="Betty-2" age="0" sex="f"/>
            <labyak name="Betty-3" age="0" sex="f"/>
            <labyak name="Betty-4" age="0" sex="f"/>
            <labyak name="Betty-5" age="9.99" sex="f"/>
        </herd>"""
        time = 5
        yak_list = query.parse_xml(xml_string)
        milk, hides = query.query(0, time, yak_list)
        self.assertEqual(hides, 1)
        self.assertEqual(milk, (50 - 999 * 0.03) + 4.0 * ((50.0 - 0.0 * 0.03) + (50.0 - 1.0 * 0.03) +
                                                        (50.0 - 2.0 * 0.03) + (50.0 - 3.0 * 0.03) + (50.0 - 4.0 * 0.03)))
        for yak in yak_list:
            if yak.name in ["Betty-1", "Betty-2", "Betty-3", "Betty-4"]:
                self.assertEqual(yak.age, 5)
            elif yak.name in ["Betty-5"] :
                self.assertEqual(yak.age, 1000)

    def test_query_6(self):
        """Test query for the yak list below"""
        yak_list = [Yak(name="Betty-1", age=413, age_last_shaved=0)]
        milk, hides = query.query(13, 14, yak_list)
        self.assertEqual(hides, 1)
        self.assertEqual(milk, (50 - 413 * 0.03))

        for yak in yak_list:
            if yak.name == "Betty-1":
                self.assertEqual(yak.age, 414)

if __name__ == '__main__':
    unittest.main()
